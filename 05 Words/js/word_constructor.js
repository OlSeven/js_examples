var TEST_INDEX = 0;
var STATUS_LINIA_WIDTH = 0;
var STATUS_LINIA_STEP = 0;
var CURRENT_INDEX = 0;

var CONTENT_MAP = JSON.parse(
	'[{"key": "fir_tree","word": "ёлка"},'+
	'{"key": "ball","word": "мячик"},'+
	'{"key": "auto","word": "машина"},'+
	'{"key": "sock","word": "носок"},'+
	'{"key": "head","word": "голова"},'+
	'{"key": "hare","word": "заяц"},'+
	'{"key": "milk","word": "молоко"},'+
	'{"key": "airplain","word": "самолёт"},'+
	'{"key": "bear","word": "медведь"},'+			
	'{"key": "cow","word": "корова"},'+
	'{"key": "crab","word": "краб"},'+
	'{"key": "eagle","word": "орёл"},'+
	
	'{"key": "elefant", "word": "слон"},'+
	'{"key": "fish","word": "рыба"},'+
	'{"key": "flower","word": "цветок"},'+
	'{"key": "fox","word": "лиса"},'+
	'{"key": "owl","word": "сова"},'+
	'{"key": "peacoack","word": "павлин"},'+
	'{"key": "pig","word": "свинья"},'+
	'{"key": "rose","word": "роза"},'+
	'{"key": "tiger","word": "тигр"},'+
	'{"key": "wolf","word": "волк"}'+
	' ]');
		
function WordConstructor() {
 
	this.feedback = function() {
		alert("CONNECTED");
	};

	this.loadIndex = function() {			
		$("#status_linia").width(STATUS_LINIA_WIDTH);
		$("#content").html('<div class="content_block"><div id="tasks_index">dfsgdfgdfsg &nbsp;</div></div><div class="content_block"><input type="button" value="Read File" onclick="readFile()"></p></div>');
		
		var list = "";
		for(var i=0; i<CONTENT_MAP.length; i++) {
			list = list+'<a onclick="(new WordConstructor()).loadTask('+i+',\''+CONTENT_MAP[i].word+'\',\''+
			CONTENT_MAP[i].key+'\');"> <img src="img/'+CONTENT_MAP[i].key+'.jpg" alt="'+CONTENT_MAP[i].key+'" height="80"></a>';
		};
		$("#tasks_index").html(list); 			
		//});					
	};

	this.initialize = function(word, key) {			
		
		var letters = [];	
		for(var i=0;i<word.length;i++) {
			var letter = {};
			letter.letter = word[i];
			letter.position = i;
			letters[i] = letter;	
		};
		
		$("#image_to_word").attr("src","img/"+key+".jpg");
		$("#image_to_word").attr("alt",key);
		$("#image_to_word").attr("style","width: 180px;");		
	
		var letters_frame_html = "";
		var input_frame_html = "";	
		var letters = this.resort(letters);
		for(var i=0; i<letters.length; i++) {	 
			letters_frame_html = letters_frame_html + "<div class='letter_frame' ondrop='drop(event)' ondragover='allowDrop(event)'><div "+"id='letter_"+letters[i].position+"' class='letter_block' draggable='true' ondragstart='drag(event)'>"+letters[i].letter+"</div></div>";
			input_frame_html = input_frame_html + "<div class='letter_frame' id='letter_frame_"+i+"' ondrop='drop(event)' ondragover='allowDrop(event)'></div>";
		};	
		$("#letters_frame").html(letters_frame_html);
		$("#input_frame").html(input_frame_html);	
		$("#task_check_button").click(function(){
			(new WordConstructor()).check(word)
		});
		STATUS_LINIA_STEP = 800/CONTENT_MAP.length;
		
	};
  
	this.loadTask = function(index, word, key) {		
		
		$("#content").html('<div class="content_block"><div id="image_frame" style="float: left; display: inline-block; width: 220px;">'+
		'<img id="image_to_word" src="img/sample.jpg" alt="Mountain View" style="width:160px;height:160px;"></div>'+
		'<div id="word_frame" style="float: left; display: inline-block; width: 500px;"><div id="letters_frame" style="width: 600px; height: 80px;">&nbsp;</div>'+
		'<div id="input_frame" style="width: 600px; height: 80px;">&nbsp;</div></div><br/><br/><br/><br/><input id="task_check_button" type="button" value="Проверить"></div>');
		
		
		CURRENT_INDEX = index;
		$("#content").load("task_content.html", function(){
			(new WordConstructor()).initialize(word, key);		
		}); 				
	};
	  
	this.resort = function(letters) {
		var res = [];
		for(var l=letters.length; l>0;l--) {
			var i = Math.floor((Math.random() * l)); 
			res.push(letters[i]);
			letters.splice(i,1);
		}  
		return res;
	};
  
	this.check = function(word) {
		var res = "";
		for(var i=0; i<word.length; i++) {
			res = res + $("#letter_frame_"+i+" div").html()
		} 
		if(word.localeCompare(res)==0) {
			STATUS_LINIA_WIDTH = STATUS_LINIA_WIDTH + STATUS_LINIA_STEP;
			$("#status_linia").width(STATUS_LINIA_WIDTH);
			CONTENT_MAP.splice(CURRENT_INDEX,1);			
			$("#true_response").dialog("open");			
		} else {
			$("#false_response").dialog("open");
		}	
	}
};

function allowDrop(ev) {
	ev.preventDefault();
};

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
};

function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	if(ev.target.className=="letter_frame"){	
		ev.target.appendChild(document.getElementById(data));
	} 
	else if(ev.target.className=="letter_block" && ev.target.parentNode.className=="letter_frame") {
		var el1 = document.getElementById(data);
		var el2 = ev.target;
		var target1 = ev.target.parentNode;
		var target2 = document.getElementById(data).parentNode;
		target1.appendChild(el1);
		target2.appendChild(el2);		  
	}
}
